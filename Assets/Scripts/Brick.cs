﻿using UnityEngine;
using System.Collections;

public class Brick : MonoBehaviour {
	public static int breakableCount = 0;
	public AudioClip crack;
	public AudioClip metal;
	public Sprite[] hitSprites;
	public GameObject smoke;
	private bool isBreakable;
	private LevelManager levelManager;
	private int timesHit;
	// Use this for initialization
	void Start () {
		isBreakable = (this.tag == "Breakable");	
		if (isBreakable) {
			breakableCount++;
		}	
		levelManager = GameObject.FindObjectOfType<LevelManager>();
		timesHit = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnCollisionExit2D (Collision2D collision) {
		if (isBreakable) {
			AudioSource.PlayClipAtPoint (crack, transform.position, 0.25f);
			HandleHits();
		} else {
			AudioSource.PlayClipAtPoint (metal, transform.position, 0.25f);
		}
	}  
	
	void PuffSmoke () {
		GameObject smokePuff = Instantiate (smoke, transform.position, Quaternion.identity) as GameObject;
		smokePuff.GetComponent<ParticleSystem>().startColor = gameObject.GetComponent<SpriteRenderer>().color;
	}
	
	void HandleHits () {
		timesHit++;
		int maxHits = hitSprites.Length + 1;
		if (timesHit >= maxHits) {
			breakableCount--;
			levelManager.BrickDestroyed();
			PuffSmoke();
			Destroy(gameObject);
		} else {
			LoadSprites();
		}
	}
	
	void LoadSprites() {
		int spriteIndex = timesHit - 1;
		if (hitSprites[spriteIndex]){
			this.GetComponent<SpriteRenderer>().sprite = hitSprites[spriteIndex];
		} else {
			Debug.LogError("Brick sprite missing.");
		}
	}
	//TODO Remove this methoded once we can actually win!
	void simulateWin () {
		levelManager.LoadNextLevel();
	}
}
