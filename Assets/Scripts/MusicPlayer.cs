﻿using UnityEngine;
using System.Collections;

public class MusicPlayer : MonoBehaviour {

	public static MusicPlayer instance = null;
	
	void Awake () {
		if (MusicPlayer.instance == null) {
			GameObject.DontDestroyOnLoad(gameObject);
			MusicPlayer.instance = this;
		} else {
			Destroy(gameObject);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
